//
//  ViewController.swift
//  BioCatchTest
//
//  Created by Refael Sommer on 20/05/2021.
//

import UIKit

class ViewController: TouchesDetector {
    
    @IBOutlet weak var cvSessions: UICollectionView!
    var controller: SessionsController = SessionsController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadSessionsList()
    }
    
    func loadSessionsList() {
        controller.arrSessions.removeAll()
        
        controller.arrSessions.append(contentsOf: DataManager.getSavedSessions())
        
        cvSessions.dataSource = controller
        cvSessions.delegate = controller
        cvSessions.reloadData()
    }

    @IBAction func openSettings(sender: UIButton) {
        guard let settingsVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC") else { return }
        settingsVC.modalPresentationStyle = .fullScreen
        self.present(settingsVC, animated: true, completion: nil)
    }
}

