//
//  TouchesDetector.swift
//  BioCatchTest
//
//  Created by Refael Sommer on 20/05/2021.
//

import Foundation
import UIKit

class TouchesDetector: UIViewController {
    var start:CGPoint?
    var startTime:TimeInterval?
    
    var kMinDistance:CGFloat   = 25.0
    var kMinDuration:CGFloat   = 0.1
    var kMinSwipeSpeed:CGFloat = 100.0
    var kMaxDoubleTapDuration:CGFloat = 1.0
    var kMinLongTapDuration:CGFloat = 0.2
    var timeFromFirstTap: TimeInterval = 0
    var locationOfFirstTap: CGPoint = CGPoint.zero
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DataManager.startSession()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let prefs = UserDefaults.standard
        kMinSwipeSpeed = CGFloat(prefs.float(forKey: "kSwipeThreshold"))
        kMaxDoubleTapDuration = CGFloat(prefs.float(forKey: "kDoubleTapThreshold"))
        kMinLongTapDuration = CGFloat(prefs.float(forKey: "kLongTapThreshold"))
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //avoid multi-touch gesture
        if(touches.count > 1){
            return;
        }
        
        if let touch:UITouch = touches.first {
            let location:CGPoint = touch.location(in: self.view)
            start = location
            startTime = touch.timestamp
            
            //Reset saved double tap info if the max duration has past or location changed
            //let dt:CGFloat = CGFloat(touch.timestamp - timeFromFirstTap)
            if timeFromFirstTap == 0 /*|| dt > kMaxDoubleTapDuration*/ || locationChanged(location: location) {
                timeFromFirstTap = touch.timestamp
                locationOfFirstTap = location
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch:UITouch = touches.first {
            let location:CGPoint = touch.location(in: self.view)
            
            let dx:CGFloat = location.x - start!.x;
            let dy:CGFloat = location.y - start!.y;
            let magnitude:CGFloat = sqrt(dx*dx+dy*dy)
            
            //print("magnitude = \(magnitude)")
            //print("locationfirst = \(locationOfFirstTap), location now = \(location)")
            let dt:CGFloat = CGFloat(touch.timestamp - startTime!)
            if (magnitude >= kMinDistance) {
                // Determine time difference from start of the gesture
                if (dt > kMinDuration) {
                    // Determine gesture speed in points/sec
                    let speed:CGFloat = magnitude / dt;
                    if (speed >= kMinSwipeSpeed) {
                        //Save fast swipe
                        print("Abnormal swipe speed = \(speed)")
                        saveGesture(type: .swipe, recordedData: speed, abnormal: true)
                    } else {
                        print("Normal swipe speed = \(speed)")
                        saveGesture(type: .swipe, recordedData: speed, abnormal: false)
                    }
                }
            }
            
            if timeFromFirstTap < startTime! &&
                    timeFromFirstTap > 0 &&
                    !locationChanged(location: location) {
                // Determine time difference from first tap to second tap
                let dt:CGFloat = CGFloat(touch.timestamp - timeFromFirstTap)
                
                if (dt <= kMaxDoubleTapDuration) {
                    print("Abnormal double tap \(dt)")
                    saveGesture(type: .doubleTap, recordedData: dt, abnormal: true)
                } else {
                    print("Normal double tap \(dt)")
                    saveGesture(type: .doubleTap, recordedData: dt, abnormal: false)
                }
                timeFromFirstTap = 0
            }
            
            if (magnitude <= 1) {
                if dt >= kMinLongTapDuration {
                    print("Abnormal long tap duration \(dt)")
                    saveGesture(type: .longTap, recordedData: dt, abnormal: true)
                } else {
                    print("Normal long tap duration \(dt)")
                    saveGesture(type: .longTap, recordedData: dt, abnormal: false)
                }
            }
            
            if timeFromFirstTap < startTime! {
                timeFromFirstTap = 0
            }
        }
    }
    
    func saveGesture(type: GestureType, recordedData: CGFloat, abnormal: Bool) {
        GestureObject.save(type: type, recordedData: Float(recordedData), abnormal: abnormal)
    }
    
    func locationChanged(location: CGPoint) -> Bool {
        if abs(locationOfFirstTap.x - location.x) < 2 &&
           abs(locationOfFirstTap.y - location.y) < 2 {
            return false
        }
        return true
    }
}
