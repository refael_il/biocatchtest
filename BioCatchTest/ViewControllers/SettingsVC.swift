//
//  SettingsVC.swift
//  BioCatchTest
//
//  Created by Refael Sommer on 21/05/2021.
//

import UIKit

class SettingsVC: UIViewController {
    
    @IBOutlet weak var lblSwipeTitle: UILabel!
    @IBOutlet weak var lblDoubleTapTitle: UILabel!
    @IBOutlet weak var lblLongTapTitle: UILabel!
    @IBOutlet weak var sliderSwipe: UISlider!
    @IBOutlet weak var sliderDoubleTap: UISlider!
    @IBOutlet weak var sliderLongTap: UISlider!
    
    let kDefaultSwipeThreshold: Float = 100
    let kDefaultDoubleTapThreshold: Float = 0.2
    let kDefaultLongTapThreshold: Float = 1
    
    let prefs = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sliderSwipe.value = prefs.object(forKey: "kSwipeThreshold") as? Float ?? kDefaultSwipeThreshold
        
        sliderDoubleTap.value = prefs.object(forKey: "kDoubleTapThreshold") as? Float ?? kDefaultDoubleTapThreshold
        
        sliderLongTap.value = prefs.object(forKey: "kLongTapThreshold") as? Float ?? kDefaultLongTapThreshold
        
        updateUI()
    }
    
    @IBAction func save(sender: UIButton) {
        prefs.setValue(sliderSwipe.value, forKey: "kSwipeThreshold")
        prefs.setValue(sliderDoubleTap.value, forKey: "kDoubleTapThreshold")
        prefs.setValue(sliderLongTap.value, forKey: "kLongTapThreshold")
        self.cancel(sender: sender)
    }
    
    @IBAction func cancel(sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func swipeValueChanged(_ sender: UISlider) {
        updateUI()
    }
    
    @IBAction func doubleTapValueChanged(_ sender: UISlider) {
        updateUI()
    }
    
    @IBAction func longTapValueChanged(_ sender: UISlider) {
        updateUI()
    }
    
    func updateUI() {
        lblSwipeTitle.text = "Swipe Threshold (\(String(format: "%.1f", sliderSwipe.value)))"
        lblDoubleTapTitle.text = "Double Tap Threshold (\(String(format: "%.1f", sliderDoubleTap.value)))"
        lblLongTapTitle.text = "Long Tap Threshold (\(String(format: "%.1f", sliderLongTap.value)))"
    }
}

