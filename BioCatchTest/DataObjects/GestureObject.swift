//
//  GestureObject.swift
//  BioCatchTest
//
//  Created by Refael Sommer on 24/05/2021.
//

import UIKit
import Foundation
import CoreData

enum GestureType: Int {
    case swipe
    case doubleTap
    case longTap
}

class GestureObject: NSManagedObject {
    var type: GestureType = .swipe
    var recordedData: Float = 0
    var session: Date = Date()
    var abnormal = false
    
    static func save(type: GestureType, recordedData: Float, abnormal: Bool) {
        var params: [String: Any] = [:]
        params["type"] = type.rawValue
        params["recordedData"] = recordedData
        params["session"] = DataManager.sessionDate
        params["abnormal"] = abnormal
        DataManager.saveData(params: params, entityName: "Gestures")
    }
}
