//
//  DataManager.swift
//  BioCatchTest
//
//  Created by Refael Sommer on 24/05/2021.
//

import UIKit
import Foundation
import CoreData

class DataManager: NSObject {
    static let sessionDate = Date()
    
    static func startSession() {
        saveData(params: ["date":sessionDate], entityName: "Sessions")
    }
    
    static func saveData(params: [String: Any], entityName: String) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        // 1
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        // 2
        let entity =
            NSEntityDescription.entity(forEntityName: entityName,
                                       in: managedContext)!
        
        let object = NSManagedObject(entity: entity,
                                     insertInto: managedContext)
        
        // 3
        for key in params.keys {
            object.setValue(params[key], forKey: key)
        }
        
        // 4
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    static func getSavedSessions() -> [Date] {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return [] }

        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Sessions")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        var sessions: [NSManagedObject] = []
        do {
            sessions = try managedContext.fetch(fetchRequest)
            var sessionDates: [Date] = []
            for sessionDate in sessions.map({ $0.value(forKey: "date") as! Date }) {
                sessionDates.append(sessionDate)
            }
            return sessionDates
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return []
    }
    
    static func getAbnormalGesturesForSession(sessionDate: Date) -> [GestureObject] {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return [] }

        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Gestures")
        fetchRequest.fetchLimit = 1;
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "recordedData", ascending: false)]
        
        var swipeGestures: [NSManagedObject] = []
        var doubleTapGestures: [NSManagedObject] = []
        var longTapGestures: [NSManagedObject] = []
        do {
            var arrGestures: [GestureObject] = []
            fetchRequest.predicate = NSPredicate(format: "session=%@ AND abnormal=true AND type=%d", sessionDate as CVarArg, GestureType.swipe.rawValue)
            swipeGestures = try managedContext.fetch(fetchRequest)
            
            fetchRequest.predicate = NSPredicate(format: "session=%@ AND abnormal=true AND type=%d", sessionDate as CVarArg, GestureType.doubleTap.rawValue)
            doubleTapGestures = try managedContext.fetch(fetchRequest)
            
            fetchRequest.predicate = NSPredicate(format: "session=%@ AND abnormal=true AND type=%d", sessionDate as CVarArg, GestureType.longTap.rawValue)
            longTapGestures = try managedContext.fetch(fetchRequest)
            
            var abnormalGestures: [NSManagedObject] = []
            abnormalGestures.append(contentsOf: swipeGestures)
            abnormalGestures.append(contentsOf: doubleTapGestures)
            abnormalGestures.append(contentsOf: longTapGestures)
            
            for gesture in abnormalGestures {
                let gestureObject = GestureObject()
                gestureObject.session = sessionDate
                gestureObject.type = GestureType.init(rawValue: gesture.value(forKey: "type") as! Int) ?? .swipe
                gestureObject.recordedData = gesture.value(forKey: "recordedData") as! Float
                gestureObject.abnormal = gesture.value(forKey: "abnormal") as! Bool
                arrGestures.append(gestureObject)
            }
            return arrGestures
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return []
    }
}
