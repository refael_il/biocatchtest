//
//  SessionsController.swift
//  BioCatchTest
//
//  Created by Refael Sommer on 24/05/2021.
//

import UIKit
import Foundation

class SessionsController: UICollectionViewController {
    var arrSessions: [Date] = []
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrSessions.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SessionCell", for: indexPath)
        cell.contentView.layer.cornerRadius = 10
        let lblSession = cell.contentView.viewWithTag(10) as! UILabel
        lblSession.text = arrSessions[indexPath.item].description
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sessionDate = arrSessions[indexPath.item]
        let arrGestures = DataManager.getAbnormalGesturesForSession(sessionDate: sessionDate)
        
        let mostAbnormalSwipeData:Float? = arrGestures.first(where: { $0.type.rawValue == GestureType.swipe.rawValue })?.recordedData
        let mostAbnormalDoubleTapData:Float? = arrGestures.first(where: { $0.type.rawValue == GestureType.doubleTap.rawValue })?.recordedData
        let mostAbnormalLongTapData:Float? = arrGestures.first(where: { $0.type.rawValue == GestureType.longTap.rawValue })?.recordedData
        
        var strAlert = "Session Date\n\(sessionDate.description)\n\n"
        if mostAbnormalSwipeData != nil {
            strAlert.append("Most abnormal swipe speed: \(mostAbnormalSwipeData!)\n")
        }
        if mostAbnormalDoubleTapData != nil {
            strAlert.append("\nMost abnormal double tap speed: \(mostAbnormalDoubleTapData!)\n")
        }
        if mostAbnormalLongTapData != nil {
            strAlert.append("\nMost abnormal long tap time: \(mostAbnormalLongTapData!)\n")
        }
        
        let alert = UIAlertController(title: "Session Details", message: strAlert, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: { action in
            alert.dismiss(animated: true, completion: nil)
        }))
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

        if var topController = keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }

            // topController should now be your topmost view controller
            topController.present(alert, animated: true, completion: nil)
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
}
